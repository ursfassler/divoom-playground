/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "DataDecoder.h"
#include "infrastructure/bytes.h"
#include <message/Elements.h>


namespace decoder
{
namespace
{


std::string enumName(uint8_t value, const std::map<uint8_t, std::string>& names)
{
  const auto idx = names.find(value);
  if (idx == names.end()) {
    return "<" + std::to_string(value) + ">";
  } else {
    return idx->second;
  }
}


}


DataDecoder::DataDecoder(const std::vector<uint8_t> &data_, std::ostream &stream_) :
  data{data_},
  stream{stream_}
{
}

std::vector<uint8_t> DataDecoder::current(const message::element::Element& value) const
{
  return {data.cbegin()+start, data.cbegin()+start+value.bytes()};
}

void DataDecoder::advance(const message::element::Element& value)
{
  start += value.bytes();
}

void DataDecoder::visit(const message::element::Command& value)
{
  stream << "  command: " << value.name() << std::endl;
  advance(value);
}

void DataDecoder::visit(const message::element::Enum& value)
{
  stream << "  " << value.name() << ": " << enumName(current(value)[0], value.values()) << std::endl;
  advance(value);
}

void DataDecoder::visit(const message::element::Byte& value)
{
  stream << "  " << value.name() << ": " << int(current(value)[0]) << std::endl;
  advance(value);
}

void DataDecoder::visit(const message::element::Const& value)
{
  stream << "  " << value.name() << ": ";
  const auto actual = current(value);
  if (actual == value.value()) {
    stream << "received expected";
  } else {
    stream << "expected " << value.value();
    stream << "; actual " << actual;
  }
  stream << std::endl;
  advance(value);
}

void DataDecoder::visit(const message::element::Pixel& value)
{
  stream << "  " << value.name() << std::endl;
  advance(value);
}

void DataDecoder::visit(const message::element::Comment& value)
{
  advance(value);
}


}
