/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <QAbstractListModel>
#include <QBluetoothDeviceDiscoveryAgent>


class DevicesModel :
    public QAbstractListModel
{
  public:

    enum Roles {
      DeviceInfoRole = Qt::UserRole,
    };

    DevicesModel();

    void startDiscovery();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  private:
    QBluetoothDeviceDiscoveryAgent discovery{};
    QList<QBluetoothDeviceInfo> devices{};

    void deviceDiscovered(QBluetoothDeviceInfo device);
};

