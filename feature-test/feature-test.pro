QT -= core
QT -= gui
CONFIG += console

TEMPLATE = app

include(../common.pri)

SOURCES += \
    output.steps.cpp \
    parser.steps.cpp \
    printer.steps.cpp \

HEADERS += \
    output.context.h \
    parser.context.h \

include($${PROJECT_ROOT}/printer/printer.pri)
include($${PROJECT_ROOT}/protocol/protocol.pri)
include($${PROJECT_ROOT}/decoder/decoder.pri)

LIBS += -lcucumber-cpp
LIBS += -lboost_regex -lboost_system -lboost_program_options -lboost_filesystem
LIBS += -pthread
LIBS += -lgtest

OTHER_FILES += \
    $${PROJECT_ROOT}/features/step_definition/cucumber.wire \
    "$${PROJECT_ROOT}/features/protocol reverse engineering.feature" \
    "$${PROJECT_ROOT}/features/protocol documentation.feature" \
