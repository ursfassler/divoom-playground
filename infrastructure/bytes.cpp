/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "bytes.h"
#include <sstream>


std::ostream& operator <<(std::ostream &stream, const std::vector<std::uint8_t> &data)
{
  stream << join(data, " ");
  return stream;
}

std::string join(const std::vector<std::uint8_t>& data, const std::string& separator)
{
  std::stringstream stream{};
  bool first = true;
  for (const auto& sym : data) {
    if (first) {
      first = false;
    } else {
      stream << separator;
    }
    stream << std::hex;
    stream.fill('0');
    stream.width(2);
    stream << int(sym);
  }
  stream << std::dec;

  return stream.str();
}
