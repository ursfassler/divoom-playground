MESSAGE_ROOT = $${PROJECT_ROOT}/message/

SOURCES *= \
    $${MESSAGE_ROOT}/element/Byte.cpp \
    $${MESSAGE_ROOT}/element/Command.cpp \
    $${MESSAGE_ROOT}/element/Const.cpp \
    $${MESSAGE_ROOT}/element/Enum.cpp \
    $${MESSAGE_ROOT}/element/Pixel.cpp \
    $$PWD/element/Comment.cpp

HEADERS *= \
    $${MESSAGE_ROOT}/element/Byte.h \
    $${MESSAGE_ROOT}/element/Command.h \
    $${MESSAGE_ROOT}/element/Const.h \
    $${MESSAGE_ROOT}/element/Element.h \
    $${MESSAGE_ROOT}/element/Enum.h \
    $${MESSAGE_ROOT}/element/Pixel.h \
    $${MESSAGE_ROOT}/Elements.h \
    $${MESSAGE_ROOT}/element/Visitor.h \
    $${MESSAGE_ROOT}/Message.h \
    $$PWD/element/Comment.h
