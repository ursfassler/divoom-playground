/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "print.h"
#include "CommandGather.h"
#include "DataOverviewPrinter.h"
#include "DataDetailPrinter.h"
#include "infrastructure/bytes.h"
#include "table.h"
#include <message/element/Element.h>


namespace printer
{


void print(std::ostream& stream, const std::map<std::string, message::Messages>& messages)
{
  for (const auto& box : messages) {
    stream << "# " << box.first << " Commands #" << std::endl;
    stream << std::endl;
    print(stream, box.second);
  }

  stream << std::endl;
}

std::size_t length(const std::vector<message::element::Element*>& value)
{
  std::size_t result = 0;

  for (const auto& itr : value) {
    result += itr->bytes();
  }

  return result;
}

void print(std::ostream& stream, const message::Messages& messages)
{
  for (const auto& message : messages) {
    CommandGather cmdPrinter{};
    for (const auto& data : message) {
      data->accept(cmdPrinter);
    }
    stream << "##";
    for (const auto& name : cmdPrinter.name) {
      stream << " " << name;
    }
    stream << " ##" << std::endl;
    stream << std::endl;

    const Table command{
      {
        {"command", table::Alignment::Left},
        {"data size", table::Alignment::Right},
      },
      {
        {
          {"command", "`" + join(cmdPrinter.command, " ") + "`"},
          {"data size", std::to_string(cmdPrinter.dataSize)},
        },
      }
    };
    print(command, stream);

    DataOverviewPrinter overview{};
    for (const auto& data : message) {
      data->accept(overview);
    }

    stream << "With the data:" << std::endl;
    stream << std::endl;
    print(overview.getTable(), stream);

    DataDetailPrinter printer{stream};
    for (const auto& data : message) {
      data->accept(printer);
    }

  }
}


}
