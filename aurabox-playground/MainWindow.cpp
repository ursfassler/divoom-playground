/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QBluetoothSocket>
#include <QBluetoothAddress>
#include <QBluetoothLocalDevice>
#include <QFileDialog>
#include <QMessageBox>
#include <QComboBox>


Q_DECLARE_METATYPE(AuraBox::Screen)
Q_DECLARE_METATYPE(AuraBox::Brightness)
Q_DECLARE_METATYPE(AuraBox::TemperatureUnit)
Q_DECLARE_METATYPE(AuraBox::Color)


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  resultDecoder{std::bind(&MainWindow::responseReceived, this, std::placeholders::_1, std::placeholders::_2)},
  messageDecoder{std::bind(&MainWindow::frameReceived, this, std::placeholders::_1), std::bind(&MainWindow::frameErrorReceived, this, std::placeholders::_1)},
  lineDecoder{std::bind(&MessageDecoder::received, &messageDecoder, std::placeholders::_1)},
  frameEncoder{std::bind(&MainWindow::sendWire, this, std::placeholders::_1)},
  messageEncoder{std::bind(&MainWindow::sendAsFrame, this, std::placeholders::_1)},
  auraBox{std::bind(&MainWindow::sendMessage, this, std::placeholders::_1, std::placeholders::_2)}
{
  ui->setupUi(this);

  QObject::connect(ui->scan, &QPushButton::clicked, this, &MainWindow::startDeviceDiscovery);
  ui->devices->setModel(&devices);

  QObject::connect(ui->connect, &QPushButton::clicked, this, &MainWindow::connect);
  QObject::connect(ui->disconnect, &QPushButton::clicked, this, &MainWindow::disconnect);
  QObject::connect(ui->sendMessage, &QPushButton::clicked, [this]{
    send(std::bind(&MainWindow::sendAsMessage, this, std::placeholders::_1));
  });
  QObject::connect(ui->sendFrame, &QPushButton::clicked, [this]{
    send(std::bind(&MainWindow::sendAsFrame, this, std::placeholders::_1));
  });
  QObject::connect(ui->sendWire, &QPushButton::clicked, [this]{
    send(std::bind(&MainWindow::sendWire, this, std::placeholders::_1));
  });

  QObject::connect(&socket, &QBluetoothSocket::connected, this, &MainWindow::connected);
  QObject::connect(&socket, &QBluetoothSocket::disconnected, this, &MainWindow::disconnected);
  QObject::connect(&socket, &QBluetoothSocket::readyRead, this, &MainWindow::readReady);

  const std::map<AuraBox::Command, std::map<uint8_t, std::string>> SimpleCommands
  {
    {AuraBox::command_SwitchScreen, AuraBox::screenNames()},
    {AuraBox::command_SetBrightness, AuraBox::brightnessNames()},
    {AuraBox::command_SetTemperatureUnit, AuraBox::temperatureUnitNames()},
    {AuraBox::command_SetColor, AuraBox::colorNames()},
  };

  for (const auto& itr : SimpleCommands) {
    const uint8_t command = uint8_t(itr.first);
    const std::string name = AuraBox::commandNames().find(itr.first)->second;
    QComboBox* box = new QComboBox();
    for (const auto& itr : itr.second) {
      box->addItem(QString::fromStdString(itr.second), QVariant::fromValue(itr.first));
    }
    simpleCommandsMap.insert(box, command);
    QObject::connect(box, SIGNAL(currentIndexChanged(int)), this, SLOT(indexChanged(int)));
    ui->simpleCommandsLayout->addRow(new QLabel(QString::fromStdString(name)), box);
  }

  QObject::connect(ui->timeGetNow, &QPushButton::clicked, this, &MainWindow::updateTime);
  QObject::connect(ui->changeTime, &QPushButton::clicked, this, &MainWindow::changeTime);

  QObject::connect(ui->imageOpen, &QPushButton::clicked, this, &MainWindow::openImage);
  QObject::connect(ui->changeImage, &QPushButton::clicked, this, &MainWindow::changeImage);

  QObject::connect(ui->changeAnimation, &QPushButton::clicked, this, &MainWindow::changeAnimation);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::startDeviceDiscovery()
{
  devices.startDiscovery();
  ui->output->append("start device discovering");
}

void MainWindow::connect()
{
  const auto index = ui->devices->currentIndex();
  const auto device = devices.data(index, DevicesModel::DeviceInfoRole).value<QBluetoothDeviceInfo>();
  const QBluetoothAddress remoteAddress = device.address();

  ui->output->append("connect to " + remoteAddress.toString());

  socket.connectToService(remoteAddress, 4);
}

void MainWindow::disconnect()
{
  socket.disconnectFromService();
}

void MainWindow::send(const std::function<void(const std::vector<uint8_t>&)>& sender)
{
  const QByteArray string = ui->input->text().toUtf8();
  const QByteArray data = QByteArray::fromHex(string);
  sender({data.data(), data.data()+data.size()});
}

void MainWindow::indexChanged(int)
{
  QComboBox* const box = dynamic_cast<QComboBox*>(sender());
  if (!simpleCommandsMap.contains(box)) {
    return;
  }
  const auto command = simpleCommandsMap.value(box);
  const auto value = box->currentData().value<uint8_t>();

  messageEncoder.send(command, {value});
}

void MainWindow::updateTime()
{
  const QDateTime now = QDateTime::currentDateTime();
  ui->timeDate->setDateTime(now);
  ui->timeTime->setDateTime(now);
}

void MainWindow::changeTime()
{
  const QDateTime time = QDateTime{ui->timeDate->date(), ui->timeTime->time()};

  AuraBox::Time s;
  s.year = time.date().year();
  s.month = AuraBox::Month(time.date().month() - 1);
  s.day = time.date().day();
  s.dayOfWeek = AuraBox::Weekday((time.date().dayOfWeek() % 7) + 1);

  s.hour = time.time().hour();
  s.minute = time.time().minute();
  s.second = time.time().second();

  auraBox.setTime(s);
}

AuraBox::Color colorBits(const QColor& value)
{
  const uint8_t r = value.red() >= 0x80 ? 0x01 : 0x00;
  const uint8_t g = value.green() >= 0x80 ? 0x02 : 0x00;
  const uint8_t b = value.blue() >= 0x80 ? 0x04 : 0x00;
  const auto color = AuraBox::Color(r | g | b);
  return color;
}

AuraBox::Image MainWindow::imageData(const QImage& image) const
{
  //TODO check width and height

  AuraBox::Image data{};
  for (uint y = 0; y < data.size(); y++) {
    for (uint x = 0; x < data[y].size(); x++) {
      const auto color = colorBits(image.pixelColor(x, y));
      data[y][x] = color;
    }
  }

  return data;
}

void MainWindow::changeImage()
{
  const QImage image = ui->image->pixmap()->toImage();
  const uint8_t width = image.width();
  const uint8_t height = image.height();

  if ((width != 10) || (height != 10)) {
    QMessageBox::critical(this, "Wrong image dimension", QString("expected an image with 10x10 pixels, got %1x%2").arg(width).arg(height));
    return;
  }

  const auto pixels = imageData(image);

  auraBox.showImage(pixels);
}

void MainWindow::openImage()
{
  const auto fileName = QFileDialog::getOpenFileName(this);
  const QPixmap image{fileName};
  ui->image->setPixmap(image);
}

void MainWindow::changeAnimation()
{
  QList<QPair<QLabel*, QSpinBox*>> anim{
    { ui->anim0, ui->delay0 },
    { ui->anim1, ui->delay1 },
    { ui->anim2, ui->delay2 },
    { ui->anim3, ui->delay3 },
    { ui->anim4, ui->delay4 },
    { ui->anim5, ui->delay5 },
    { ui->anim6, ui->delay6 },
    { ui->anim7, ui->delay7 },
  };

  AuraBox::Animation animation{};

  for (uint8_t i = 0; i < anim.size(); i++) {
    const QImage image = anim[i].first->pixmap()->toImage();
    const uint8_t width = image.width();
    const uint8_t height = image.height();

    if ((width != 10) || (height != 10)) {
      QMessageBox::critical(this, "Wrong image dimension", QString("expected an image with 10x10 pixels, got %1x%2").arg(width).arg(height));
      return;
    }

    const auto pixels = imageData(image);
    const AuraBox::Frame frame{pixels, anim[i].second->value()};

    animation.push_back(frame);
  }

  auraBox.showAnimation(animation);
}

void MainWindow::connected()
{
  ui->output->append("connected");
}

void MainWindow::disconnected()
{
  ui->output->append("disconnected");
}

void MainWindow::readReady()
{
  while (socket.bytesAvailable() > 0) {
    const auto data = socket.readAll();
    const uint8_t* start = reinterpret_cast<const uint8_t*>(data.data());
    const std::vector<uint8_t> vdata{start, start+data.size()};
    ui->output->append("recv raw (" + QString::number(data.size()) + "): " + toHex(vdata));
    lineDecoder.received(vdata);
  }
}

void MainWindow::frameReceived(const std::vector<uint8_t> &value)
{
  ui->output->append("recv message: " + toHex(value));
  resultDecoder.received(value);
}

void MainWindow::frameErrorReceived(uint8_t command)
{
  ui->output->append(QString("frame error for command %1").arg(command, 2, 16));
}

void MainWindow::responseReceived(uint8_t command, const std::vector<uint8_t>& data)
{
  QStringList hexData{};
  for (uint8_t sym : data) {
    hexData.append(QString("%1").arg(sym, 2, 16));
  }
  ui->output->append(QString("response command=%1 data=%2").arg(command, 2, 16).arg(hexData.join(" ")));
}

void MainWindow::sendMessage(uint8_t command, const std::vector<uint8_t>& data)
{
  ui->output->append(QString("send message command=%1 data=%2").arg(qlonglong(command), 2, 16).arg(toHex(data)));
  messageEncoder.send(command, data);
}

void MainWindow::sendAsMessage(const std::vector<uint8_t>& value)
{
  if (value.size() < 1) {
    QMessageBox::critical(this, "not enough data", QString("Message needs at least the command"));
    return;
  }

  const uint8_t command = value.front();
  const std::vector<uint8_t> data{value.cbegin()+1, value.cend()};

  sendMessage(command, data);
}

void MainWindow::sendAsFrame(const std::vector<uint8_t>& value)
{
  ui->output->append("send frame (" + QString::number(value.size()) + "): " + toHex(value));
  frameEncoder.send(value);
}

void MainWindow::sendWire(const FrameEncoder::Data& value)
{
  ui->output->append("send wire (" + QString::number(value.size()) + "): " + toHex(value));
  socket.write(reinterpret_cast<const char*>(value.data()), value.size());
}

QString MainWindow::toHex(const std::vector<uint8_t>& value) const
{
  const QByteArray array = QByteArray::fromRawData(reinterpret_cast<const char*>(value.data()), value.size());
  return array.toHex(' ');
}
