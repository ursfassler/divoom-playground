/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <functional>


class ResultDecoder
{
  public:
    typedef std::function<void(uint8_t, const std::vector<uint8_t>&)> ResponseHandler;

    ResultDecoder(const ResponseHandler&);

    void received(const std::vector<uint8_t>&);

  private:
    const ResponseHandler response;
};

