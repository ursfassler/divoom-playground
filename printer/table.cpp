/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "table.h"


namespace printer
{
namespace
{

struct ColumnInfo
{
    std::string name;
    table::Alignment alignment;
    std::size_t width;
};

std::string getContent(const std::string& column, const table::ContentColumn& row)
{
  const auto idx = row.find(column);
  return (idx == row.end()) ? "" : idx->second;
}

}


void print(const Table& data, std::ostream& stream)
{
  if (data.header.empty()) {
    return;
  }

  std::vector<ColumnInfo> columnInfo;

  for (const auto& column : data.header) {
    columnInfo.push_back({
                           column.name,
                           column.alignment,
                           column.name.size(),
                         });
  }
  for (auto& row : data.content) {
    for (auto& column : columnInfo) {
      const auto content = getContent(column.name, row);
      column.width = std::max(column.width, content.size());
    }
  }

  stream << "|";
  for (const auto& column : columnInfo) {
    const auto fillCount = column.width - column.name.size();
    const std::string fill(fillCount, ' ');

    stream << " ";
    stream << column.name;
    stream << fill;
    stream << " ";
    stream << "|";
  }
  stream << std::endl;

  stream << "|";
  for (const auto& column : columnInfo) {
    stream << "-";
    stream << std::string(column.width, '-');
    stream << ((column.alignment == table::Alignment::Left) ? "-" : ":");
    stream << "|";
  }
  stream << std::endl;

  for (const auto& row : data.content) {
    stream << "|";
    for (const auto& column : columnInfo) {
      const auto content = getContent(column.name, row);
      const auto fillCount = column.width - content.size();
      const std::string fill(fillCount, ' ');

      stream << ((column.alignment == table::Alignment::Left) ? "" : fill);
      stream << " " << content << " ";
      stream << ((column.alignment == table::Alignment::Right) ? "" : fill);
      stream << "|";
    }
    stream << std::endl;
  }

  stream << std::endl;
}


}
