/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <cstdint>


constexpr uint8_t StartOfFrame = 0x01;
constexpr uint8_t EndOfFrame = 0x02;
constexpr uint8_t EscapeCharacter = 0x03;
