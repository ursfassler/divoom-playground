/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "CommandMatcher.h"
#include <message/element/Element.h>
#include <message/Elements.h>


namespace decoder
{


CommandMatcher::CommandMatcher(const std::vector<uint8_t> &data_) :
  data{data_}
{
}

uint8_t CommandMatcher::current() const
{
  return data[start];
}

void CommandMatcher::advance(const message::element::Element& value)
{
  start += value.bytes();
}

void CommandMatcher::visit(const message::element::Command& value)
{
  matches &= current() == value.value();
  advance(value);
}

void CommandMatcher::visit(const message::element::Enum& value)
{
  advance(value);
}

void CommandMatcher::visit(const message::element::Byte& value)
{
  advance(value);
}

void CommandMatcher::visit(const message::element::Const& value)
{
  advance(value);
}

void CommandMatcher::visit(const message::element::Pixel& value)
{
  advance(value);
}

void CommandMatcher::visit(const message::element::Comment& value)
{
  advance(value);
}


}
