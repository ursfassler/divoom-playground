/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "CommandGather.h"
#include <message/Elements.h>


namespace printer
{


void CommandGather::visit(const message::element::Command& value)
{
  command.push_back(value.value());
  name.push_back(value.name());
}

void CommandGather::visit(const message::element::Enum& value)
{
  dataSize += value.bytes();
}

void CommandGather::visit(const message::element::Byte& value)
{
  dataSize += value.bytes();
}

void CommandGather::visit(const message::element::Const& value)
{
  dataSize += value.bytes();
}

void CommandGather::visit(const message::element::Pixel& value)
{
  dataSize += value.bytes();
}

void CommandGather::visit(const message::element::Comment& value)
{
  dataSize += value.bytes();
}


}
