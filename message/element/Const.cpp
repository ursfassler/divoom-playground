/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Const.h"
#include "Visitor.h"


namespace message::element
{


Const::Const(const std::string &name__, const std::vector<uint8_t> &value__) :
  name_{name__},
  value_{value__}
{
}

std::size_t Const::bytes() const
{
  return value_.size();
}

std::string Const::name() const
{
  return name_;
}

std::vector<uint8_t> Const::value() const
{
  return value_;
}

void Const::accept(Visitor& value) const
{
  value.visit(*this);
}


}
