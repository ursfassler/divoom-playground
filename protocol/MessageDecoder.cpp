/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "MessageDecoder.h"


MessageDecoder::MessageDecoder(const TelegramHandler& telegramHandler_, const FrameErrorHandler& frameErrorHandler_) :
  telegramHandler{telegramHandler_},
  frameErrorHandler{frameErrorHandler_}
{
}

uint16_t read16(const std::vector<uint8_t>& data, int index)
{
  const uint16_t low = data[index];
  const uint16_t high = data[index+1];
  const uint16_t value = low | (high << 8);
  return value;
}

void MessageDecoder::received(const std::vector<uint8_t>& value)
{
  if (value.size() < 2) {
    return;
  }

  uint16_t sum = 0;
  for (std::size_t i = 0; i < value.size()-2; i++) {
    sum += value[i];
  }

  const uint16_t recvSum = read16(value, value.size() - 2);

  if (sum != recvSum) {
    return;
  }

  const auto recvSize = read16(value, 0);
  if (recvSize != value.size()-2) {
    if ((value.size() == 4) && (value[1] == 0xaa)) {
      const uint8_t command = value[0];
      frameErrorHandler(command);
    }

    return;
  }

  const auto payloadSize = value.size() - 4;
  if (payloadSize < 1) {  // we expect at least a command
    return;
  }

  //TODO check that length is enough
  const auto beginContent = 2;
  const auto endContent = value.size()-2;
  const std::vector<uint8_t> content{value.cbegin()+beginContent, value.cbegin()+endContent};
  telegramHandler(content);
}
