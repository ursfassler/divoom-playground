#-------------------------------------------------
#
# Project created by QtCreator 2018-12-17T20:35:00
#
#-------------------------------------------------

QT -= core
QT -= gui
CONFIG += console

TEMPLATE = app

include(../common.pri)

SOURCES += \
    $${PROJECT_ROOT}/protocol/FrameDecoder.cpp \
    $${PROJECT_ROOT}/protocol/FrameDecoder.test.cpp \
    $${PROJECT_ROOT}/protocol/MessageDecoder.cpp \
    $${PROJECT_ROOT}/protocol/MessageDecoder.test.cpp \
    $${PROJECT_ROOT}/protocol/ResultDecoder.cpp \
    $${PROJECT_ROOT}/protocol/ResultDecoder.test.cpp \

HEADERS += \
    $${PROJECT_ROOT}/protocol/FrameDecoder.h \
    $${PROJECT_ROOT}/protocol/Framing.h \
    $${PROJECT_ROOT}/protocol/MessageDecoder.h \
    $${PROJECT_ROOT}/protocol/ResultDecoder.h \

LIBS += -lgmock_main -lgmock -lgtest
