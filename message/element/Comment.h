/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "Element.h"
#include <vector>

namespace message::element
{


class Comment :
    public Element
{
  public:
    typedef std::string Line;
    typedef std::vector<Line> Paragraph;

    Comment(const Paragraph&);

    Paragraph text() const;
    std::size_t bytes() const override;
    std::string name() const override;
    void accept(Visitor& value) const override;

  private:
    const Paragraph text_;

};


}
