/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/Message.h>
#include <map>


namespace printer
{

void print(std::ostream& stream, const std::map<std::string, message::Messages> &);
void print(std::ostream& stream, const message::Messages &messages);

}
