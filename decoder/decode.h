/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/Message.h>
#include <vector>
#include <map>


namespace decoder
{


void decode(const std::vector<uint8_t>&, std::ostream&, const std::map<std::string, message::Messages>&);


}
