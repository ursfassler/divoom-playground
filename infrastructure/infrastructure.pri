INFRASTRUCTURE_ROOT = $${PROJECT_ROOT}/infrastructure/

SOURCES *= \
    $${INFRASTRUCTURE_ROOT}/bytes.cpp \

HEADERS *= \
    $${INFRASTRUCTURE_ROOT}/bytes.h \
