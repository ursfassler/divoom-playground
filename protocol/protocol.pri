PROTOCOL_ROOT = $${PROJECT_ROOT}/protocol/

include($${PROJECT_ROOT}/message/message.pri)

SOURCES *= \
    $${PROTOCOL_ROOT}/AuraBox.cpp \
    $${PROTOCOL_ROOT}/FrameDecoder.cpp \
    $${PROTOCOL_ROOT}/FrameEncoder.cpp \
    $${PROTOCOL_ROOT}/MessageDecoder.cpp \
    $${PROTOCOL_ROOT}/MessageEncoder.cpp \
    $${PROTOCOL_ROOT}/Messages.cpp \
    $${PROTOCOL_ROOT}/ResultDecoder.cpp \

HEADERS *= \
    $${PROTOCOL_ROOT}/AuraBox.h \
    $${PROTOCOL_ROOT}/FrameDecoder.h \
    $${PROTOCOL_ROOT}/FrameEncoder.h \
    $${PROTOCOL_ROOT}/Framing.h \
    $${PROTOCOL_ROOT}/MessageDecoder.h \
    $${PROTOCOL_ROOT}/MessageEncoder.h \
    $${PROTOCOL_ROOT}/Messages.h \
    $${PROTOCOL_ROOT}/ResultDecoder.h \

