/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "protocol/FrameDecoder.h"
#include "protocol/MessageDecoder.h"
#include "protocol/ResultDecoder.h"
#include "DevicesModel.h"
#include "protocol/FrameEncoder.h"
#include "protocol/MessageEncoder.h"
#include "protocol/AuraBox.h"
#include <QMainWindow>
#include <QBluetoothSocket>
#include <QBluetoothDeviceDiscoveryAgent>


namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

  private slots:
    void indexChanged(int index);

  private:
    Ui::MainWindow *ui;
    QBluetoothSocket socket{QBluetoothServiceInfo::RfcommProtocol};
    ResultDecoder resultDecoder;
    MessageDecoder messageDecoder;
    FrameDecoder lineDecoder;
    DevicesModel devices;
    FrameEncoder frameEncoder;
    MessageEncoder messageEncoder;
    AuraBox auraBox;
    QMap<QObject*, uint8_t> simpleCommandsMap{};

    void startDeviceDiscovery();
    void connect();
    void disconnect();
    void send(const std::function<void(const std::vector<uint8_t>&)>&);

    void connected();
    void disconnected();
    void readReady();
    void frameReceived(const std::vector<uint8_t>&);
    void frameErrorReceived(uint8_t);
    void responseReceived(uint8_t, const std::vector<uint8_t>&);

    void sendMessage(uint8_t command, const std::vector<uint8_t>&);
    void sendAsMessage(const std::vector<uint8_t>&);
    void sendAsFrame(const std::vector<uint8_t> &);
    void sendWire(const FrameEncoder::Data&);
    QString toHex(const std::vector<uint8_t>&) const;

    void updateTime();
    void changeTime();
    void changeImage();
    void openImage();
    void changeAnimation();

    AuraBox::Image imageData(const QImage &image) const;
};
