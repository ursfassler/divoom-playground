# language: en

Feature: reverse engineering of the AuraBox protocol
  As a developer
  I want to understand the AuraBox protocol
  In order to be able to control the AuraBox with my own application


Scenario: decode switch screen from frame

  When I receive the frame "01 04 00 45 03 05 4b 00 02"

  Then I expect to see the following output:
    """
    received 45 02
    AuraBox
      command: Switch Screen
      screen: Light
    ----

    """


Scenario Outline: command switch screen

  When I receive the message "<message>"

  Then I expect to see the following output:
    """
    received <message>
    AuraBox
      command: Switch Screen
      screen: <screen>
    ----

    """

  Examples:
    | message | screen                             |
    | 45 00   | Time                               |
    | 45 01   | Temperature                        |
    | 45 02   | Light                              |
    | 45 03   | Equalizer                          |
    | 45 04   | Discover the boundless imagination |
    | 45 05   | Party rock                         |
    | 45 06   | Soar to the rainbow                |
    | 45 07   | Fuel the stellar explosion         |


Scenario: command set brightness

  When I receive the message "32 3f"

  Then I expect to see the following output:
    """
    received 32 3f
    AuraBox
      command: Set Brightness
      brightness: Dark
    ----

    """


Scenario: command set color

  When I receive the message "47 03"

  Then I expect to see the following output:
    """
    received 47 03
    AuraBox
      command: Set Color
      color: Orange
    ----

    """


Scenario: command set temperature unit

  When I receive the message "4c 00"

  Then I expect to see the following output:
    """
    received 4c 00
    AuraBox
      command: Set Temperature Unit
      unit: Celsius
    ----

    """


Scenario: command set time

  When I receive the message "18 12 14 0b 1c 0c 00 1a 06"

  Then I expect to see the following output:
    """
    received 18 12 14 0b 1c 0c 00 1a 06
    AuraBox
      command: Set Time
      year last 2 digits: 18
      year first 2 digits: 20
      month: December
      day: 28
      hour: 12
      minute: 0
      second: 26
      day of week: Friday
    ----

    """


Scenario: command show image

  When I receive the message "44 00 0a 0a 04 10 42 53 76 60 00 00 00 00 60 00 04 00 00 60 00 40 00 00 00 00 00 04 00 00 00 00 40 00 00 00 00 00 04 00 11 01 00 30 33 11 01 00 30 33 11 01 00 30 33"

  Then I expect to see the following output:
    """
    received 44 00 0a 0a 04 10 42 53 76 60 00 00 00 00 60 00 04 00 00 60 00 40 00 00 00 00 00 04 00 00 00 00 40 00 00 00 00 00 04 00 11 01 00 30 33 11 01 00 30 33 11 01 00 30 33
    AuraBox
      command: Show Image
      header: received expected
      pixels
    ----

    """


Scenario: command show animation

  When I receive the message "49 00 0a 0a 04 06 05 66 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"

  Then I expect to see the following output:
    """
    received 49 00 0a 0a 04 06 05 66 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 06 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    AuraBox
      command: Show Animation
      header: received expected
      frame number: 6
      delay: 5
      pixels
    ----

    """
