/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Byte.h"
#include "Visitor.h"


namespace message::element
{


Byte::Byte(const std::string &name__) :
  name_{name__}
{
}

std::size_t Byte::bytes() const
{
  return 1;
}

std::string Byte::name() const
{
  return name_;
}

void Byte::accept(Visitor& value) const
{
  value.visit(*this);
}


}
