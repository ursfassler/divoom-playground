/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>


namespace message
{
namespace element
{

class Element;

}

typedef std::vector<element::Element*> Message;
typedef std::vector<Message> Messages;


}
