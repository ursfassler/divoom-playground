PRINTER_ROOT = $${PROJECT_ROOT}/printer/

include($${PROJECT_ROOT}/message/message.pri)
include($${PROJECT_ROOT}/infrastructure/infrastructure.pri)

SOURCES *= \
    $${PRINTER_ROOT}/CommandGather.cpp \
    $${PRINTER_ROOT}/DataDetailPrinter.cpp \
    $${PRINTER_ROOT}/DataOverviewPrinter.cpp \
    $${PRINTER_ROOT}/print.cpp \
    $${PRINTER_ROOT}/table.cpp \

HEADERS *= \
    $${PRINTER_ROOT}/CommandGather.h \
    $${PRINTER_ROOT}/DataDetailPrinter.h \
    $${PRINTER_ROOT}/DataOverviewPrinter.h \
    $${PRINTER_ROOT}/print.h \
    $${PRINTER_ROOT}/table.h \
