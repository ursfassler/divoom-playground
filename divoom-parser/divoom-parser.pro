QT -= core
QT -= gui

TARGET = divoom-parser
TEMPLATE = app

include(../common.pri)

SOURCES += \
    main.cpp \

HEADERS += \

include($${PROJECT_ROOT}/protocol/protocol.pri)
include($${PROJECT_ROOT}/decoder/decoder.pri)
