/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "ResultDecoder.h"


ResultDecoder::ResultDecoder(const ResultDecoder::ResponseHandler& response_) :
  response{response_}
{
}

void ResultDecoder::received(const std::vector<uint8_t>& value)
{
  if (value.size() < 3) {
    return;
  }

  const uint8_t command = value[0];
  const uint8_t requestCommand = value[1];
  const uint8_t state = value[2];

  if (command != 0x04) {
    throw std::runtime_error("expected command 04");
  }
  if (state != 0x55) {
    throw std::runtime_error("expected state 55");
  }

  response(requestCommand, {value.cbegin()+3, value.cend()});
}
