/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Command.h"
#include "Visitor.h"


namespace message::element
{


Command::Command(const std::string &name__, uint8_t value__) :
  name_{name__},
  value_{value__}
{
}

std::size_t Command::bytes() const
{
  return 1;
}

std::string Command::name() const
{
  return name_;
}

uint8_t Command::value() const
{
  return value_;
}

void Command::accept(Visitor &value) const
{
  value.visit(*this);
}


}
