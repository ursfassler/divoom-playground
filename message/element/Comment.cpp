/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Comment.h"
#include "Visitor.h"


namespace message::element
{


Comment::Comment(const Paragraph& text__) :
  text_{text__}
{
}

Comment::Paragraph Comment::text() const
{
  return text_;
}

std::size_t Comment::bytes() const
{
  return 0;
}

std::string Comment::name() const
{
  return {};
}

void Comment::accept(Visitor& value) const
{
  value.visit(*this);
}


}
