/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <functional>


class FrameDecoder
{
  public:
    typedef std::function<void(const std::vector<uint8_t>&)> TelegramHandler;

    FrameDecoder(const TelegramHandler&);

    void received(const std::vector<uint8_t>&);

  private:
    const TelegramHandler handler;
    std::vector<uint8_t> data{};

    void received(uint8_t);

    enum class State
    {
      Idle,
      Content,
      Escaped,
    };

    State state{State::Idle};

};
