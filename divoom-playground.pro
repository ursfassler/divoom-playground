TEMPLATE = subdirs

SUBDIRS = \
    aurabox-playground \
    unit-test \
    divoom-parser \
    feature-test \
    divoom-protocol-documentation \
