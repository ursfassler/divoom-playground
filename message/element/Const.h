/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "Element.h"
#include <vector>


namespace message::element
{


class Const :
    public Element
{
  public:
    Const(const std::string&, const std::vector<uint8_t>&);

    std::size_t bytes() const override;
    std::string name() const override;
    std::vector<uint8_t> value() const;
    void accept(Visitor& value) const override;

  private:
    const std::string name_;
    const std::vector<uint8_t> value_;

};


}
