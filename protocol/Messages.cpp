/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Messages.h"
#include "AuraBox.h"
#include <message/Elements.h>


message::element::Command* command(AuraBox::Command value)
{
  const auto idx = AuraBox::commandNames().find(value);
  if (idx == AuraBox::commandNames().end()) {
    throw std::runtime_error("no command name for " + std::to_string(value));
  }
  return new message::element::Command(idx->second, value);
}

const message::Messages AuraBox {
  {
    command(AuraBox::command_SwitchScreen),
    new message::element::Enum("screen", AuraBox::screenNames()),
  },
  {
    command(AuraBox::command_SetBrightness),
    new message::element::Enum("brightness", AuraBox::brightnessNames()),
  },
  {
    command(AuraBox::command_SetTemperatureUnit),
    new message::element::Enum("unit", AuraBox::temperatureUnitNames()),
  },
  {
    command(AuraBox::command_SetColor),
    new message::element::Enum("color", AuraBox::colorNames()),
    new message::element::Comment({"A color value are 3 bits for the colors red (bit 0), green (bit 1) and blue (bit 2)."}),
  },
  {
    command(AuraBox::command_SetTime),
    new message::element::Byte("year last 2 digits"),
    new message::element::Byte("year first 2 digits"),
    new message::element::Enum("month", AuraBox::monthNames()),
    new message::element::Byte("day"),
    new message::element::Byte("hour"),
    new message::element::Byte("minute"),
    new message::element::Byte("second"),
    new message::element::Enum("day of week", AuraBox::weekdayNames()),
    new message::element::Comment({"It seems that only the hour, minute and seconds are used by the device."}),
  },
  {
    command(AuraBox::command_ShowImage),
    new message::element::Const("header", {0, 10, 10, 4}),
    new message::element::Pixel("pixels", 50),
  },
  {
    command(AuraBox::command_ShowAnimation),
    new message::element::Const("header", {0, 10, 10, 4}),
    new message::element::Byte("frame number"),
    new message::element::Byte("delay"),
    new message::element::Pixel("pixels", 50),
    new message::element::Comment({
                                    "Animation frames have to be sent one after the other, from frame number 0 to the highest frame number (7 or less).",
                                    "The frame is show for about (0.2 + delay * 0.1) seconds.",
                                  }),
  },
};

const std::map<std::string, message::Messages> AllMessages {
  { "AuraBox", AuraBox },
};


const std::map<std::string, message::Messages> &messages()
{
  return AllMessages;
}
