/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "output.context.h"
#include <printer/print.h>
#include <message/Elements.h>
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace
{


WHEN("^I print the Test documentation$")
{
  const std::map<uint8_t, std::string> Names
  {
    {10, "name 1"},
    {20, "name 2"},
  };
  const std::map<std::string, message::Messages> messages
  {
    { "Test", {
      {
        new message::element::Command("The Command", 0x42),
        new message::element::Command("subcommand", 0x57),
        new message::element::Enum("param 1", Names),
        new message::element::Byte("byte"),
        new message::element::Const("fill", {0x0a, 0x0b, 0x0c}),
        new message::element::Comment({
                                        "This is a comment block.",
                                        "With a second line.",
                                      }),
      },
      {
        new message::element::Command("Command 2", 0x02),
        new message::element::Enum("p1", {
                                     {0, "first"},
                                     {1, "second"},
                                     {2, "third"},
                                   }),
        new message::element::Enum("p2", {
                                     {0, "off"},
                                     {1, "on"},
                                   }),
        new message::element::Pixel("p3", 50),
      },
    }},
  };

  cucumber::ScenarioScope<output::Context> context;
  printer::print(context->output, messages);
}


}
