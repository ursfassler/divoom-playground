/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Enum.h"
#include "Visitor.h"


namespace message::element
{


Enum::Enum(const std::string &name__, const std::map<uint8_t, std::string>& values__) :
  name_{name__},
  values_{values__}
{
}

std::size_t Enum::bytes() const
{
  return 1;
}

std::string Enum::name() const
{
  return name_;
}

std::map<uint8_t, std::string> Enum::values() const
{
  return values_;
}

void Enum::accept(Visitor &value) const
{
  value.visit(*this);
}


}
