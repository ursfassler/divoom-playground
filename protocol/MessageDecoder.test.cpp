/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "MessageDecoder.h"

#include <gmock/gmock.h>


namespace
{

using namespace testing;


class MessageDecoder_Test :
    public Test
{
public:
    MessageDecoder testee{
      std::bind(&MessageDecoder_Test::messageDecoded, this, std::placeholders::_1),
      std::bind(&MessageDecoder_Test::frameError, this, std::placeholders::_1)
    };

    std::vector<uint8_t> data{};
    void messageDecoded(const std::vector<uint8_t>& data)
    {
      this->data = data;
    }

    uint8_t frameErrorFor{};
    void frameError(uint8_t command)
    {
      frameErrorFor = command;
    }

    std::vector<uint8_t> a(const std::vector<uint8_t>& value) const
    {
      return value;
    }
};


TEST_F(MessageDecoder_Test, decode_empty_telegram)
{

  testee.received({0x02, 0x00, 0x02, 0x00});

  ASSERT_EQ(a({}), data);
}

TEST_F(MessageDecoder_Test, decode_telegram_with_some_data)
{

  testee.received({0x05, 0x00, 0x10, 0x20, 0x30, 0x65, 0x00});

  ASSERT_EQ(a({0x10, 0x20, 0x30}), data);
}

TEST_F(MessageDecoder_Test, decode_telegram_with_large_checksum)
{

  testee.received({0x05, 0x00, 0xff, 0xff, 0xff, 0x02, 0x03});

  ASSERT_EQ(a({0xff, 0xff, 0xff}), data);
}

TEST_F(MessageDecoder_Test, decode_large_telegram)
{
  std::vector<uint8_t> raw{};
  raw.push_back(0x02);
  raw.push_back(0x01);
  for (unsigned i = 0; i < 256; i++) {
    raw.push_back(0);
  }
  raw.push_back(0x03);
  raw.push_back(0x00);

  testee.received(raw);

  ASSERT_EQ(256, data.size());
}

TEST_F(MessageDecoder_Test, decode_an_answer)
{

  testee.received({0x06, 0x00, 0x04, 0x32, 0x55, 0xc4, 0x55, 0x01});

  ASSERT_EQ(a({0x04, 0x32, 0x55, 0xc4}), data);
}

TEST_F(MessageDecoder_Test, decode_a_frame_error_answer)
{

  testee.received({0x32, 0xaa, 0xdc, 0x00});

  ASSERT_EQ(0x32, frameErrorFor);
}


}
