/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "FrameDecoder.h"
#include "Framing.h"


FrameDecoder::FrameDecoder(const TelegramHandler& handler_) :
  handler{handler_}
{
}

void FrameDecoder::received(const std::vector<uint8_t>& value)
{
  for (auto sym : value) {
    received(sym);
  }
}

void FrameDecoder::received(uint8_t value)
{
  switch (state) {
    case State::Idle:
      switch (value) {
        case StartOfFrame:
          data.clear();
          state = State::Content;
          break;

        default:
          state = State::Idle;
          break;
      }
      break;

    case State::Content:
      switch (value) {
        case StartOfFrame:
          data.clear();
          state = State::Content;
          break;

        case EndOfFrame:
          state = State::Idle;
          handler(data);
          break;

        case EscapeCharacter:
          state = State::Escaped;
          break;

        default:
          data.push_back(value);
          state = State::Content;
          break;
      }
      break;


    case State::Escaped:
      switch (value) {
        case StartOfFrame:
          data.clear();
          state = State::Content;
          break;

        case EndOfFrame:
          state = State::Idle;
          break;

        default:
          data.push_back(value - 0x03);
          state = State::Content;
          break;
      }
      break;
  }
}
