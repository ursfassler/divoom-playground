/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/element/Visitor.h>
#include <ostream>


namespace printer
{


class DataDetailPrinter :
    public message::element::Visitor
{
  public:
    DataDetailPrinter(std::ostream& stream_);

    void visit(const message::element::Command&) override;
    void visit(const message::element::Enum&) override;
    void visit(const message::element::Byte&) override;
    void visit(const message::element::Const&) override;
    void visit(const message::element::Pixel&) override;
    void visit(const message::element::Comment&) override;

  private:
    std::ostream& stream;

};


}
