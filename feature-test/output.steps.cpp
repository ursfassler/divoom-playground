/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "output.context.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace
{


THEN("^I expect to see the following output:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<output::Context> context;
  const auto actual = context->output.str();
  ASSERT_EQ(expected, actual);
}


}
