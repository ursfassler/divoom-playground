DECODER_ROOT = $${PROJECT_ROOT}/decoder/

include($${PROJECT_ROOT}/message/message.pri)
include($${PROJECT_ROOT}/infrastructure/infrastructure.pri)

SOURCES *= \
    $${DECODER_ROOT}/CommandMatcher.cpp \
    $${DECODER_ROOT}/DataDecoder.cpp \
    $${DECODER_ROOT}/decode.cpp \

HEADERS *= \
    $${DECODER_ROOT}/CommandMatcher.h \
    $${DECODER_ROOT}/DataDecoder.h \
    $${DECODER_ROOT}/decode.h \

