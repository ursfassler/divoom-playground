/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Pixel.h"
#include "Visitor.h"


namespace message::element
{


Pixel::Pixel(const std::string &name__, std::size_t count__) :
  name_{name__},
  count_{count__}
{
}

std::size_t Pixel::bytes() const
{
  return count_;
}

std::string Pixel::name() const
{
  return name_;
}

void Pixel::accept(Visitor& value) const
{
  value.visit(*this);
}


}
