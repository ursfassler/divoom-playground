/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "protocol/FrameDecoder.h"
#include "protocol/MessageDecoder.h"
#include "parser.context.h"
#include <message/Elements.h>
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace
{


std::vector<uint8_t> parseBytes(const std::string& value)
{
  std::vector<uint8_t> result{};

  std::stringstream stream{value};
  stream << std::hex;
  while (!stream.eof()) {
    int symbol;
    stream >> symbol;
    result.push_back(symbol);
  }

  return result;
}

WHEN("^I receive the frame \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, raw);
  const std::vector<uint8_t> received = parseBytes(raw);

  cucumber::ScenarioScope<parser::Context> context;
  context->frameDecoder.received(received);
}

WHEN("^I receive the message \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, raw);
  const std::vector<uint8_t> received = parseBytes(raw);

  cucumber::ScenarioScope<parser::Context> context;
  decoder::decode(received, context->outputContext->output, messages()); //TODO do better
}


}
