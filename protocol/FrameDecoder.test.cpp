/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "FrameDecoder.h"

#include <gmock/gmock.h>


namespace
{

using namespace testing;


class FrameDecoder_Test :
    public Test
{
public:
    FrameDecoder testee{std::bind(&FrameDecoder_Test::frameDecoded, this, std::placeholders::_1)};

    std::vector<std::vector<uint8_t>> frames{};

    void frameDecoded(const std::vector<uint8_t>& value)
    {
      frames.push_back(value);
    }

    std::vector<uint8_t> a(const std::vector<uint8_t>& value) const
    {
      return value;
    }
};


TEST_F(FrameDecoder_Test, decode_empty_frame)
{

  testee.received({0x01, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({}), frames[0]);
}

TEST_F(FrameDecoder_Test, decode_some_data)
{

  testee.received({0x01, 0x10, 0x20, 0x30, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x10, 0x20, 0x30}), frames[0]);
}

TEST_F(FrameDecoder_Test, decode_two_frame)
{

  testee.received({0x01, 0x10, 0x02, 0x01, 0x20, 0x02});

  ASSERT_EQ(2, frames.size());
  ASSERT_EQ(a({0x10}), frames[0]);
  ASSERT_EQ(a({0x20}), frames[1]);
}

TEST_F(FrameDecoder_Test, ignore_data_before_start)
{

  testee.received({0x20, 0x30, 0x01, 0x10, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x10}), frames[0]);
}

TEST_F(FrameDecoder_Test, ignore_data_after_end)
{

  testee.received({0x01, 0x10, 0x02, 0x20, 0x30});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x10}), frames[0]);
}

TEST_F(FrameDecoder_Test, ignore_data_between_frames)
{

  testee.received({0x01, 0x02, 0x10, 0x01, 0x02});

  ASSERT_EQ(2, frames.size());
  ASSERT_EQ(a({}), frames[0]);
  ASSERT_EQ(a({}), frames[1]);
}

TEST_F(FrameDecoder_Test, ignore_multiple_start_of_frame)
{

  testee.received({0x01, 0x01, 0x01, 0x10, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x10}), frames[0]);
}

TEST_F(FrameDecoder_Test, ignore_multiple_end_of_frame)
{

  testee.received({0x01, 0x10, 0x02, 0x02, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x10}), frames[0]);
}

TEST_F(FrameDecoder_Test, decode_escaped_symbols)
{

  testee.received({0x01, 0x03, 0x04, 0x03, 0x05, 0x03, 0x06, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x01, 0x02, 0x03}), frames[0]);
}

TEST_F(FrameDecoder_Test, decode_an_answer)
{

  testee.received({0x01, 0x06, 0x00, 0x04, 0x32, 0x55, 0xc4, 0x55, 0x03, 0x04, 0x02});

  ASSERT_EQ(1, frames.size());
  ASSERT_EQ(a({0x06, 0x00, 0x04, 0x32, 0x55, 0xc4, 0x55, 0x01}), frames[0]);
}


}
