/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace output
{


class Context
{
  public:
    std::stringstream output{};
};


}
