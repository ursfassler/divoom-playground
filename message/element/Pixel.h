/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "Element.h"


namespace message::element
{


class Pixel :
    public Element
{
  public:
    Pixel(const std::string&, std::size_t);

    std::size_t bytes() const override;
    std::string name() const override;
    void accept(Visitor& value) const override;

  private:
    const std::string name_;
    const std::size_t count_;

};


}
