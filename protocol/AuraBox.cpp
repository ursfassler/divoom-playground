/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "AuraBox.h"


namespace aurabox
{


const std::map<uint8_t, std::string> CommandNames
{
  {AuraBox::command_SwitchScreen, "Switch Screen"},
  {AuraBox::command_SetBrightness, "Set Brightness"},
  {AuraBox::command_SetTemperatureUnit, "Set Temperature Unit"},
  {AuraBox::command_SetColor, "Set Color"},
  {AuraBox::command_SetTime, "Set Time"},
  {AuraBox::command_ShowImage, "Show Image"},
  {AuraBox::command_ShowAnimation, "Show Animation"},
};

const std::map<uint8_t, std::string> ScreenNames
{
  {AuraBox::screen_time, "Time"},
  {AuraBox::screen_temperature, "Temperature"},
  {AuraBox::screen_light, "Light"},
  {AuraBox::screen_equalizer, "Equalizer"},
  {AuraBox::screen_discover_the_boundless_imagination, "Discover the boundless imagination"},
  {AuraBox::screen_party_rock, "Party rock"},
  {AuraBox::screen_soar_to_the_rainbow, "Soar to the rainbow"},
  {AuraBox::screen_fuel_the_stellar_explosion, "Fuel the stellar explosion"},
};

const std::map<uint8_t, std::string> BrightnessNames
{
  {AuraBox::brightness_off, "Off"},
  {AuraBox::brightness_dark, "Dark"},
  {AuraBox::brightness_light, "Light"},
};

const std::map<uint8_t, std::string> TemperatureUnitNames
{
  {AuraBox::temperatureunit_celsius, "Celsius"},
  {AuraBox::temperatureunit_farenheit, "Farenheit"},
};

const std::map<uint8_t, std::string> ColorNames
{
  {AuraBox::color_black, "Black"},
  {AuraBox::color_red, "Red"},
  {AuraBox::color_green, "Green"},
  {AuraBox::color_orange, "Orange"},
  {AuraBox::color_blue, "Blue"},
  {AuraBox::color_purple, "Purple"},
  {AuraBox::color_cyan, "Cyan"},
  {AuraBox::color_white, "White"},
};

const std::map<uint8_t, std::string> MonthNames
{
  {AuraBox::month_January, "January"},
  {AuraBox::month_February, "February"},
  {AuraBox::month_March, "March"},
  {AuraBox::month_April, "April"},
  {AuraBox::month_May, "May"},
  {AuraBox::month_June, "June"},
  {AuraBox::month_July, "July"},
  {AuraBox::month_August, "August"},
  {AuraBox::month_September, "September"},
  {AuraBox::month_October, "October"},
  {AuraBox::month_November, "November"},
  {AuraBox::month_December, "December"},
};

const std::map<uint8_t, std::string> WeekdayNames
{
  {AuraBox::weekday_Sunday, "Sunday"},
  {AuraBox::weekday_Monday, "Monday"},
  {AuraBox::weekday_Tuesday, "Tuesday"},
  {AuraBox::weekday_Wednesday, "Wednesday"},
  {AuraBox::weekday_Thursday, "Thursday"},
  {AuraBox::weekday_Friday, "Friday"},
  {AuraBox::weekday_Saturday, "Saturday"},
};


}


AuraBox::AuraBox(const AuraBox::Sender& sendMessage_) :
  sendMessage{sendMessage_}
{
}

const std::map<uint8_t, std::string> &AuraBox::commandNames()
{
  return aurabox::CommandNames;
}

const std::map<uint8_t, std::string> &AuraBox::screenNames()
{
  return aurabox::ScreenNames;
}

void AuraBox::switchScreen(Screen value)
{
  sendMessage(command_SwitchScreen, {uint8_t(value)});
}

const std::map<uint8_t, std::string> &AuraBox::brightnessNames()
{
  return aurabox::BrightnessNames;
}

void AuraBox::setBrightness(Brightness value)
{
  sendMessage(command_SetBrightness, {uint8_t(value)});
}

const std::map<uint8_t, std::string> &AuraBox::temperatureUnitNames()
{
  return aurabox::TemperatureUnitNames;
}

void AuraBox::setTemperatureUnit(TemperatureUnit value)
{
  sendMessage(command_SetTemperatureUnit, {uint8_t(value)});
}

const std::map<uint8_t, std::string> &AuraBox::colorNames()
{
  return aurabox::ColorNames;
}

void AuraBox::setColor(Color value)
{
  sendMessage(command_SetColor, {uint8_t(value)});
}

const std::map<uint8_t, std::string> &AuraBox::monthNames()
{
  return aurabox::MonthNames;
}

const std::map<uint8_t, std::string> &AuraBox::weekdayNames()
{
  return aurabox::WeekdayNames;
}

void AuraBox::setTime(const Time& value)
{
  const std::vector<uint8_t> data{
    uint8_t(value.year % 100),
    uint8_t(value.year / 100),
    value.month,
    value.day,

    value.hour,
    value.minute,
    value.second,

    value.dayOfWeek,
  };

  sendMessage(command_SetTime, data);
}

void AuraBox::showImage(const Image& value)
{
  const auto header = imageHeader();
  const auto pixels = imageData(value);

  std::vector<uint8_t> data{};
  data.insert(data.end(), header.cbegin(), header.cend());
  data.insert(data.end(), pixels.cbegin(), pixels.cend());

  sendMessage(command_ShowImage, data);
}

void AuraBox::showAnimation(const Animation& value)
{
  //TODO check number of frames

  for (uint8_t i = 0; i < value.size(); i++) {
    const auto header = imageHeader();
    const auto pixels = imageData(value[i].first);

    std::vector<uint8_t> data{};
    data.insert(data.end(), header.cbegin(), header.cend());
    data.push_back(i);
    data.push_back(value[i].second);  // ca. 0.2 sec + val * 0.1 sec
    data.insert(data.end(), pixels.cbegin(), pixels.cend());

    sendMessage(command_ShowAnimation, data);
  }
}

std::vector<uint8_t> AuraBox::imageData(const Image& image) const
{
  std::vector<uint8_t> data{};

  for (uint y = 0; y < image.size(); y++) {
    for (uint x = 0; x < image[y].size()/2; x++) {
      const Color color1 = image[y][2*x];
      const Color color2 = image[y][2*x+1];
      const uint8_t symbol = uint8_t(color1) | (uint8_t(color2) << 4);
      data.push_back(symbol);
    }
  }

  return data;
}

std::vector<uint8_t> AuraBox::imageHeader() const
{
  return {
    0x00,   // ignored by AuraBox
    10,     // ignored by AuraBox
    10,     // ignored by AuraBox
    0x04,   // ignored by AuraBox
  };
}
