/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <functional>


class MessageDecoder
{
  public:
    typedef std::function<void(const std::vector<uint8_t>&)> TelegramHandler;
    typedef std::function<void(uint8_t)> FrameErrorHandler;

    MessageDecoder(const TelegramHandler&, const FrameErrorHandler&);

    void received(const std::vector<uint8_t>&);

  private:
    const TelegramHandler telegramHandler;
    const FrameErrorHandler frameErrorHandler;

};
