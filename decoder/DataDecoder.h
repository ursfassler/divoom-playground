/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/element/Visitor.h>
#include <vector>
#include <cstdint>
#include <ostream>


namespace message::element
{

class Element;

}

namespace decoder
{


class DataDecoder :
    public message::element::Visitor
{
  public:
    DataDecoder(const std::vector<uint8_t>&, std::ostream&);

    std::size_t start{0};
    std::vector<uint8_t> data;
    std::ostream& stream;

    std::vector<uint8_t> current(const message::element::Element&) const;
    void advance(const message::element::Element&);

    void visit(const message::element::Command&) override;
    void visit(const message::element::Enum&) override;
    void visit(const message::element::Byte&) override;
    void visit(const message::element::Const&) override;
    void visit(const message::element::Pixel&) override;
    void visit(const message::element::Comment&) override;

};


}
