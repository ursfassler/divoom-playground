#include "protocol/FrameDecoder.h"
#include "protocol/MessageDecoder.h"
#include "protocol/ResultDecoder.h"
#include "protocol/Messages.h"
#include "decoder/decode.h"
#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


void printCommandError(uint8_t command)
{
  std::cout << std::hex;
  std::cout.fill('0');
  std::cout.width(2);

  std::cout << "error in message with command " << int(command);
}

int main(int argc, const char* argv[])
{
  const std::vector<std::string> arg{argv, argv+argc};

  if (arg.size() != 2) {
    std::cerr << "need exactly 1 argument: <filename to read from>" << std::endl;
    return 1;
  }

  const auto filename = arg[1];
  auto& stream = std::cout;

  MessageDecoder messageDecoder{[&stream](const std::vector<uint8_t>& data){
      decoder::decode(data, stream, messages());
    }, printCommandError};
  FrameDecoder frameDecoder{std::bind(&MessageDecoder::received, &messageDecoder, std::placeholders::_1)};

  const int fd = ::open(filename.c_str(), O_RDONLY);
  if (fd < 0) {
    std::cerr << "could not open file: " << filename << std::endl;
    return 2;
  }

  while (true) {
    uint8_t buffer[128];
    const int read = ::read(fd, buffer, sizeof(buffer));
    if (read <= 0) {
      break;
    }
    frameDecoder.received({buffer, buffer+read});
  }

  ::close(fd);

  return 0;
}
