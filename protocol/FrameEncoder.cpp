/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "FrameEncoder.h"
#include "Framing.h"


FrameEncoder::FrameEncoder(const FrameEncoder::Writer& writer_) :
  writer{writer_}
{
}

void FrameEncoder::send(const Data& value)
{
  Data encoded{};
  encoded.push_back(StartOfFrame);

  for (const auto sym : value) {
    switch (sym) {
      case StartOfFrame:
      case EndOfFrame:
      case EscapeCharacter:
        encoded.push_back(EscapeCharacter);
        encoded.push_back(sym + 0x03);
        break;

      default:
        encoded.push_back(sym);
        break;
    }
  }

  encoded.push_back(EndOfFrame);
  writer(encoded);
}
