/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <ostream>
#include <vector>


std::string join(const std::vector<std::uint8_t>&, const std::string&);
std::ostream& operator <<(std::ostream&, const std::vector<std::uint8_t>&);
