/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <functional>


class MessageEncoder
{
  public:
    typedef uint8_t Command;
    typedef std::vector<uint8_t> Data;
    typedef std::vector<uint8_t> Encoded;
    typedef std::function<void(const Encoded&)> Writer;

    MessageEncoder(const Writer&);

    void send(Command, const Data&);

  private:
    const Writer writer;

};
