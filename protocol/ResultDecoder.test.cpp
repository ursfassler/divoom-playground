/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "ResultDecoder.h"

#include <gmock/gmock.h>


namespace
{

using namespace testing;


class ResultDecoder_Test :
    public Test
{
public:
    ResultDecoder testee{std::bind(&ResultDecoder_Test::response, this, std::placeholders::_1, std::placeholders::_2)};

    uint8_t command{};
    std::vector<uint8_t> data{};

    void response(uint8_t command, const std::vector<uint8_t>& data)
    {
      this->command = command;
      this->data = data;
    }

    std::vector<uint8_t> a(const std::vector<uint8_t>& value) const
    {
      return value;
    }
};


TEST_F(ResultDecoder_Test, decode_an_empty_answer)
{

  testee.received({0x04, 0x45, 0x55});

  ASSERT_EQ(0x45, command);
  ASSERT_EQ(a({}), data);
}

TEST_F(ResultDecoder_Test, decode_an_answer_with_data)
{

  testee.received({0x04, 0x45, 0x55, 0x10, 0x20, 0x30});

  ASSERT_EQ(0x45, command);
  ASSERT_EQ(a({0x10, 0x20, 0x30}), data);
}


}
