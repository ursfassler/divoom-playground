# language: en

Feature: document the AuraBox protocol
  As a developer
  I want to see easily see the AuraBox protocol
  In order to be able to control the AuraBox with my own application


Scenario: print simple command

  When I print the Test documentation

  Then I expect to see the following output:
    """
    # Test Commands #

    ## The Command subcommand ##

    | command | data size |
    |---------|----------:|
    | `42 57` |         5 |

    With the data:

    | data    | size |
    |---------|-----:|
    | param 1 |    1 |
    | byte    |    1 |
    | fill    |    3 |

    With param 1:

    | value | param 1 |
    |------:|---------|
    |    10 | name 1  |
    |    20 | name 2  |

    Where fill is `0a 0b 0c`

    This is a comment block.
    With a second line.

    ## Command 2 ##

    | command | data size |
    |---------|----------:|
    | `02`    |        52 |

    With the data:

    | data | size |
    |------|-----:|
    | p1   |    1 |
    | p2   |    1 |
    | p3   |   50 |

    With p1:

    | value | p1     |
    |------:|--------|
    |     0 | first  |
    |     1 | second |
    |     2 | third  |

    With p2:

    | value | p2  |
    |------:|-----|
    |     0 | off |
    |     1 | on  |

    p3 are the pixels from top left to the bottom right, ordered in rows.
    One pixel is represented by half a byte.
    Red is in bit 0, green in bit 1 and blue in bit 2.
    2 pixels are sent in one byte, the first pixel in the lower nibble.



    """
