/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "protocol/FrameDecoder.h"
#include "protocol/MessageDecoder.h"
#include "protocol/Messages.h"
#include "output.context.h"
#include <decoder/decode.h>
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace parser
{


class Context
{
  public:
    cucumber::ScenarioScope<output::Context> outputContext;
    MessageDecoder messageDecoder{[this](const std::vector<uint8_t>& value){
        decoder::decode(value, outputContext->output, messages());
      }, [](uint8_t){}};
    FrameDecoder frameDecoder{std::bind(&MessageDecoder::received, &messageDecoder, std::placeholders::_1)};

};


}
