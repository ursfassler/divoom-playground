/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "Element.h"


namespace message::element
{


class Command :
    public Element
{
  public:
    Command(const std::string&, uint8_t);

    std::size_t bytes() const override;
    std::string name() const override;
    uint8_t value() const;
    void accept(Visitor& value) const override;

  private:
    const std::string name_;
    const uint8_t value_;
};


}
