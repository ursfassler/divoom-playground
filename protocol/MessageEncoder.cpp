/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "MessageEncoder.h"


MessageEncoder::MessageEncoder(const MessageEncoder::Writer& writer_) :
  writer{writer_}
{
}

void MessageEncoder::send(MessageEncoder::Command command, const MessageEncoder::Data& data)
{
  std::vector<uint8_t> encoded{};

  const uint16_t length = 2 + 1 + data.size() ;
  const uint8_t lengthHigher = (length >> 8) & 0xff;
  const uint8_t lengthLower = length & 0xff;

  encoded.push_back(lengthLower);
  encoded.push_back(lengthHigher);
  encoded.push_back(command);
  encoded.insert(encoded.end(), data.cbegin(), data.cend());

  uint16_t checksum = 0;
  for (const auto sym : encoded) {
    checksum += uint8_t(sym);
  }

  const uint8_t checksumHigher = (checksum >> 8) & 0xff;
  const uint8_t checksumLower = checksum & 0xff;
  encoded.push_back(checksumLower);
  encoded.push_back(checksumHigher);

  writer(encoded);
}
