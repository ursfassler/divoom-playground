# Description #

Here you find code and tools to play around with [Divoom smart](http://divoom.com/article/lists/category/7.html) devices.
The main goal is to analyze and reverse engineer the protocol in order to control the LED screen from self made applications.

Start playing around with the [main project file](divoom-playground.pro) or the [CI file](.gitlab-ci.yml).

# Tests #

The [feature](features) and [unit](unit-test) tests are executed on each commit.

[![pipeline status](https://gitlab.com/ursfassler/divoom-playground/badges/master/pipeline.svg)](https://gitlab.com/ursfassler/divoom-playground/commits/master)
[![coverage report](https://gitlab.com/ursfassler/divoom-playground/badges/master/coverage.svg)](https://gitlab.com/ursfassler/divoom-playground/commits/master)

# Protocol #

The protocol description is partially automatically generated from the code.
It can be downloaded from the [artifacts](https://gitlab.com/ursfassler/divoom-playground/-/jobs/artifacts/master/download?job=all).

## Reverse engineering ##

Feed the [divoom-parser](divoom-parser) with the data from an [app](https://play.google.com/store/apps/details?id=com.divoom.Divoom&hl=en_US) that can control the device.

A way to get the data is with the [rfcomm-server](https://gitlab.com/ursfassler/rfcomm-server).
The divoom apps connect to server when the Bluetooth name is recognized from the app.
Therefore, name the device where the rfcomm-server is running something like _AuraBox-light_ or _TimeBox-light_.

When sending commands with the app, the parser decodes those commands.
New commands can be added in [protocol](protocol).
After adding the commands, they are used by the parser and included in the protocol documentation.

