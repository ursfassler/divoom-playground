/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <functional>


class FrameEncoder
{
  public:
    typedef std::vector<uint8_t> Data;
    typedef std::function<void(const Data&)> Writer;

    FrameEncoder(const Writer&);

    void send(const Data&);

  private:
    const Writer writer;

};
