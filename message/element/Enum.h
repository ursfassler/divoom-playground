/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "Element.h"
#include <map>


namespace message::element
{


class Enum :
    public Element
{
  public:
    Enum(const std::string&, const std::map<uint8_t, std::string>&);

    std::size_t bytes() const override;
    std::string name() const override;
    std::map<uint8_t, std::string> values() const;
    void accept(Visitor& value) const override;

  private:
    const std::string name_;
    const std::map<uint8_t, std::string> values_;

};


}
