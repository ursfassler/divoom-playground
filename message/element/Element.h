/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <string>

namespace message::element
{

class Visitor;


class Element
{
  public:
    virtual std::size_t bytes() const = 0;
    virtual std::string name() const = 0;
    virtual void accept(Visitor&) const = 0;

};


}
