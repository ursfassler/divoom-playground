/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/Message.h>
#include <map>


const std::map<std::string, message::Messages> &messages();
