/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <functional>
#include <vector>
#include <map>


class AuraBox
{
  public:
    typedef std::function<void(uint8_t, const std::vector<uint8_t>&)> Sender;

    AuraBox(const Sender&);

    enum Command : uint8_t
    {
      command_SwitchScreen = 0x45,
      command_SetBrightness = 0x32,
      command_SetTemperatureUnit = 0x4c,
      command_SetColor = 0x47,
      command_SetTime = 0x18,
      command_ShowImage = 0x44,
      command_ShowAnimation = 0x49,
    };
    static const std::map<uint8_t, std::string>& commandNames();

    enum Screen : uint8_t
    {
      screen_time = 0,
      screen_temperature = 1,
      screen_light = 2,
      screen_equalizer = 3,
      screen_discover_the_boundless_imagination = 4,
      screen_party_rock = 5,
      screen_soar_to_the_rainbow = 6,
      screen_fuel_the_stellar_explosion = 7,
    };
    static const std::map<uint8_t, std::string>& screenNames();
    void switchScreen(Screen);

    enum Brightness : uint8_t
    {
      brightness_off = 0x00,
      brightness_dark = 0x3f,
      brightness_light = 0xd2,
    };
    static const std::map<uint8_t, std::string>& brightnessNames();
    void setBrightness(Brightness);

    enum TemperatureUnit : uint8_t
    {
      temperatureunit_celsius = 0,
      temperatureunit_farenheit = 1,
    };
    static const std::map<uint8_t, std::string>& temperatureUnitNames();
    void setTemperatureUnit(TemperatureUnit);

    enum Color : uint8_t
    {
      color_black = 0x00,
      color_red = 0x01,
      color_green = 0x02,
      color_orange = 0x03,
      color_blue = 0x04,
      color_purple = 0x05,
      color_cyan = 0x06,
      color_white = 0x07,
    };
    static const std::map<uint8_t, std::string>& colorNames();
    void setColor(Color);

    enum Month : uint8_t
    {
      month_January = 0,
      month_February = 1,
      month_March = 2,
      month_April = 3,
      month_May = 4,
      month_June = 5,
      month_July = 6,
      month_August = 7,
      month_September = 8,
      month_October = 9,
      month_November = 10,
      month_December = 11,
    };
    static const std::map<uint8_t, std::string>& monthNames();
    enum Weekday : uint8_t
    {
      weekday_Sunday = 1,
      weekday_Monday = 2,
      weekday_Tuesday = 3,
      weekday_Wednesday = 4,
      weekday_Thursday = 5,
      weekday_Friday = 6,
      weekday_Saturday = 7,
    };
    static const std::map<uint8_t, std::string>& weekdayNames();
    struct Time
    {
        uint16_t year;
        Month month;
        uint8_t day;
        uint8_t hour;
        uint8_t minute;
        uint8_t second;
        Weekday dayOfWeek;
    };
    void setTime(const Time&);

    typedef std::array<std::array<Color, 10>, 10> Image;
    void showImage(const Image&);

    typedef std::pair<Image, uint8_t> Frame;
    typedef std::vector<Frame> Animation;
    void showAnimation(const Animation&);

  private:
    const Sender sendMessage;
    std::vector<uint8_t> imageData(const Image &image) const;
    std::vector<uint8_t> imageHeader() const;
};
