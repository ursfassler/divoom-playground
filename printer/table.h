/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <vector>
#include <map>
#include <string>
#include <ostream>


namespace printer
{
namespace table
{


enum class Alignment
{
  Left,
  Right,
};

struct HeaderColumn
{
    std::string name;
    Alignment alignment;
};

typedef std::vector<HeaderColumn> Header;
typedef std::map<std::string, std::string> ContentColumn;
typedef std::vector<ContentColumn> Content;


}

struct Table
{
    table::Header header;
    table::Content content;
};

void print(const Table&, std::ostream&);


}
