/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "decode.h"
#include "CommandMatcher.h"
#include "DataDecoder.h"
#include "infrastructure/bytes.h"
#include <message/element/Element.h>
#include <ostream>


namespace decoder
{


void decode(const std::vector<uint8_t>& data, std::ostream& stream, const std::map<std::string, std::vector<message::Message>>& messages)
{
  stream << "received " << data << std::endl;

  for (const auto& box : messages) {
    stream << box.first << std::endl;
    for (const auto command : box.second) {
      CommandMatcher matcher{data};
      for (const auto& itr : command) {
        itr->accept(matcher);
      }

      if (matcher.matches) {
        DataDecoder v{data, stream};
        for (const auto& itr : command) {
          itr->accept(v);
        }
        if (v.start != v.data.size()) {
          stream << "  wrong data length, expected " << v.start << ", actual " << v.data.size() << std::endl;
        }
      }
    }
  }

  stream << "----" << std::endl;
}


}
