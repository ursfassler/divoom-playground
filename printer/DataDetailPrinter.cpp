/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "DataDetailPrinter.h"
#include "infrastructure/bytes.h"
#include "table.h"
#include <message/Elements.h>
#include <ostream>
#include <algorithm>


namespace printer
{


DataDetailPrinter::DataDetailPrinter(std::ostream &stream_) :
  stream{stream_}
{
}

void DataDetailPrinter::visit(const message::element::Command& value)
{
}

void DataDetailPrinter::visit(const message::element::Enum& value)
{
  const auto name = value.name();
  Table table;
  table.header = {
    { "value", table::Alignment::Right },
    { name, table::Alignment::Left },
  };
  for (const auto& itr : value.values()) {
    table.content.push_back({
                              { "value", std::to_string(itr.first) },
                              { name, itr.second },
                            });
  }

  stream << "With " << name << ":" << std::endl;
  stream << std::endl;
  print(table, stream);
}

void DataDetailPrinter::visit(const message::element::Byte& value)
{
}

void DataDetailPrinter::visit(const message::element::Const& value)
{
  stream << "Where " << value.name() << " is `" << join(value.value(), " ") << "`";
  stream << std::endl;
  stream << std::endl;
}

void DataDetailPrinter::visit(const message::element::Pixel& value)
{
  stream << value.name() << " are the pixels from top left to the bottom right, ordered in rows." << std::endl;
  stream << "One pixel is represented by half a byte." << std::endl;
  stream << "Red is in bit 0, green in bit 1 and blue in bit 2." << std::endl;
  stream << "2 pixels are sent in one byte, the first pixel in the lower nibble." << std::endl;
  stream << std::endl;
}

void DataDetailPrinter::visit(const message::element::Comment& value)
{
  for (const auto& line : value.text()) {
    stream << line << std::endl;
  }
  stream << std::endl;
}


}
