/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "DevicesModel.h"


DevicesModel::DevicesModel()
{
  connect(&discovery, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &DevicesModel::deviceDiscovered);
}

void DevicesModel::startDiscovery()
{
  devices.clear();
  discovery.start();
}

void DevicesModel::deviceDiscovered(QBluetoothDeviceInfo device)
{
  beginInsertRows({}, devices.size(), devices.size());
  devices.append(device);
  endInsertRows();
}

int DevicesModel::rowCount(const QModelIndex &parent) const
{
  return devices.size();
}

QVariant DevicesModel::data(const QModelIndex &index, int role) const
{
  switch (role) {
    case Qt::DisplayRole:
      return devices[index.row()].name();

    case DeviceInfoRole:
      return QVariant::fromValue(devices[index.row()]);
  }

  return {};
}
