/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "table.h"
#include <message/element/Visitor.h>
#include <ostream>


namespace message::element
{

class Element;

}


namespace printer
{


class DataOverviewPrinter :
    public message::element::Visitor
{
  public:
    DataOverviewPrinter();

    void visit(const message::element::Command&) override;
    void visit(const message::element::Enum&) override;
    void visit(const message::element::Byte&) override;
    void visit(const message::element::Const&) override;
    void visit(const message::element::Pixel&) override;
    void visit(const message::element::Comment&) override;

    const Table& getTable() const;

  private:
    void print(const message::element::Element&);
    Table table;

};


}
