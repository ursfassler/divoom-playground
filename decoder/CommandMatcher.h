/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include "message/element/Visitor.h"
#include <vector>
#include <cstdint>


namespace message::element
{

class Element;

}


namespace decoder
{


class CommandMatcher :
    public message::element::Visitor
{
  public:
    CommandMatcher(const std::vector<uint8_t>&);

    bool matches{true};
    std::size_t start{0};
    std::vector<uint8_t> data;

    uint8_t current() const;
    void advance(const message::element::Element&);

    void visit(const message::element::Command&) override;
    void visit(const message::element::Enum&) override;
    void visit(const message::element::Byte&) override;
    void visit(const message::element::Const&) override;
    void visit(const message::element::Pixel&) override;
    void visit(const message::element::Comment&) override;

};


}
