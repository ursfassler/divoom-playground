/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "DataOverviewPrinter.h"
#include <message/Elements.h>


namespace printer
{
namespace
{

const std::string Data{"data"};
const std::string Size{"size"};

}

DataOverviewPrinter::DataOverviewPrinter()
{
  table.header = {
    {Data, table::Alignment::Left},
    {Size, table::Alignment::Right},
  };
}

void DataOverviewPrinter::visit(const message::element::Command&)
{
}

void DataOverviewPrinter::visit(const message::element::Enum& value)
{
  print(value);
}

void DataOverviewPrinter::visit(const message::element::Byte& value)
{
  print(value);
}

void DataOverviewPrinter::visit(const message::element::Const& value)
{
  print(value);
}

void DataOverviewPrinter::visit(const message::element::Pixel& value)
{
  print(value);
}

void DataOverviewPrinter::visit(const message::element::Comment&)
{
}

void DataOverviewPrinter::print(const message::element::Element& value)
{
  table.content.push_back({
                            {Data, value.name()},
                            {Size, std::to_string(value.bytes())},
                          });
}

const Table& DataOverviewPrinter::getTable() const
{
  return table;
}


}
