/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once


namespace message::element
{


class Command;
class Enum;
class Byte;
class Const;
class Pixel;
class Comment;


class Visitor
{
  public:
    virtual void visit(const Command&) = 0;
    virtual void visit(const Enum&) = 0;
    virtual void visit(const Byte&) = 0;
    virtual void visit(const Const&) = 0;
    virtual void visit(const Pixel&) = 0;
    virtual void visit(const Comment&) = 0;

};


}
