/*
 * (C) Copyright 2018 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <message/element/Visitor.h>
#include <vector>
#include <string>


namespace printer
{


class CommandGather :
    public message::element::Visitor
{
  public:
    std::vector<uint8_t> command{};
    std::vector<std::string> name{};
    std::size_t dataSize{0};

    void visit(const message::element::Command&) override;
    void visit(const message::element::Enum&) override;
    void visit(const message::element::Byte&) override;
    void visit(const message::element::Const&) override;
    void visit(const message::element::Pixel&) override;
    void visit(const message::element::Comment&) override;

};


}
