CONFIG += c++17
CONFIG += warn_on
QT -= core
QT -= gui

TARGET = divoom-protocol-documentation
TEMPLATE = app

include(../common.pri)

SOURCES += \
    main.cpp \

HEADERS += \

include($${PROJECT_ROOT}/printer/printer.pri)
include($${PROJECT_ROOT}/protocol/protocol.pri)
